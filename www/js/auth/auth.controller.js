// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('auth');


// creation du controleur
app.controller('LoginCtrl', function($scope, Auth, $state) {

  Auth.$signInWithPopup('google')
  .then(function(firebaseUser) {
    console.log(Auth.$getAuth());
    $state.go('chatList');

  }).catch(function(error){
    console.log("Authentification failed :", error);
  });

});