// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('chat');

app.config(function($stateProvider) {    
    $stateProvider
        
        .state('chatList', {
            url: '/chatList',
            controller: 'ChatListCtrl',
            templateUrl: 'js/chat/chatList/chatList.html'
        })
        .state('addChat', {
            url: '/addChat',
            controller: 'addChatModalCtrl',
            templateUrl: 'js/chat/addChat/addChat.html'
        })
        .state('discussion', {
            url: '/chat/:id',
            controller: 'DiscussionCtrl',
            templateUrl: 'js/chat/discussion/discussion.html'
        });
});