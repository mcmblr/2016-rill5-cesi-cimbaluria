var app = angular.module("chat");


app.controller("ChatListCtrl", ["$scope", "Auth", "$firebaseArray",
  function($scope, Auth, $firebaseArray){
    var messagesRef = firebase.database().ref();
    $scope.messages = $firebaseArray(messagesRef);

  $scope.date = new Date().toISOString();

  $scope.messages.$loaded().then(function(items) {
    console.log(items);
      if (items.length === 0)
      {
        //Nothing
        $scope.messages.$add({
        nom: "Electro",
        date: $scope.date,
        user: Auth.$getAuth().displayName
        });
        $scope.messages.$add({
        nom: "Pop",
        date: $scope.date,
        user: Auth.$getAuth().displayName
        });
        $scope.messages.$add({
        nom: "KPOP",
        date: $scope.date,
        user: Auth.$getAuth().displayName
        });
        $scope.messages.$add({
        nom: "Classique",
        date: $scope.date,
        user: Auth.$getAuth().displayName
        });
      }
  });

  }
]);

app.controller('addChatModalCtrl', function($scope, Auth, $state, $ionicModal, $firebaseArray) {

$ionicModal.fromTemplateUrl('js/chat/addChat/addChat.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.date = new Date().toISOString();
    $scope.createSalon = function(u) {
    var messagesRef = firebase.database().ref();
    $scope.messages = $firebaseArray(messagesRef);   
    $scope.messages.$add({
        nom: u.nomSalon,
        date: $scope.date,
        user: Auth.$getAuth().displayName,
        messages: []
        });
    $scope.modal.hide();
    $state.go('chatList');

  };
  $scope.openModal = function() {
    $scope.modal.show();
  };

}
);


app.controller('DiscussionCtrl', function($scope, Auth, $stateParams, $firebaseArray, $state){


    // on récupère les paramètres dans l'url (la conversation et l'utilisateur)
    console.log($stateParams.id);
    $scope.id = $stateParams.id;
    
    // on créer une connexion à notre base de donnée Firebase
    var tchatRef = firebase.database().ref().child($scope.id).child('messages');
    $scope.tchat = $firebaseArray(tchatRef);

    // quand l'utilisateur clique sur envoyer
    // on ajoute le message dans la base de donnée Firebase
    $scope.add = function(add){
      $scope.tchat.$add({
        "date": new Date().getTime(),
        "user": Auth.$getAuth().displayName,
        "content": add.message
      });
      $scope.add.message = "";
    };
  });





