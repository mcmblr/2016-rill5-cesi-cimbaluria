var app = angular.module("chat");

app.factory("chatMessages", ["$firebaseArray",
  function($firebaseArray) {
    // create a reference to the database where we will store our data
    var ref = firebase.database().ref();


    return $firebaseArray(ref);
  }
]);

app.factory("ChatManager", ["$firebaseObject", "$firebaseArray",
  function($firebaseObject, $firebaseArray) {
    //constructeur de l'objet ChatManager
    var ChatManager = function(id) {
    	this.id = id;
    };

    //récupère l'objet qui contient les membres
    ChatManager.prototype.getMembers = function(){
    	return $firebaseObject(firebase.database().ref().child('members/'+this.id));
    };

    //récupère le chat
    ChatManager.prototype.getChat = function(){
    	//@todo
    };

    ChatManager.prototype.getMessages = function(number){

    };

  return ChatManager;
}]);